/**
 *  Eternity Keeper, a Pillars of Eternity save game editor.
 *  Copyright (C) 2015 Kim Mantas
 *
 *  Eternity Keeper is free software: you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  Eternity Keeper is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package uk.me.mantas.eternity.tests.save;

import com.google.common.primitives.UnsignedInteger;
import org.apache.commons.io.FileUtils;
import org.cef.callback.CefQueryCallback;
import org.jooq.lambda.tuple.Tuple2;
import org.json.JSONObject;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import uk.me.mantas.eternity.EKUtils;
import uk.me.mantas.eternity.Environment;
import uk.me.mantas.eternity.Logger;
import uk.me.mantas.eternity.Settings;
import uk.me.mantas.eternity.factory.PacketDeserializerFactory;
import uk.me.mantas.eternity.game.ComponentPersistencePacket;
import uk.me.mantas.eternity.game.CurrencyValue;
import uk.me.mantas.eternity.game.ObjectPersistencePacket;
import uk.me.mantas.eternity.save.SavedGameOpener;
import uk.me.mantas.eternity.serializer.PacketDeserializer;
import uk.me.mantas.eternity.serializer.properties.Property;
import uk.me.mantas.eternity.tests.ExposedClass;
import uk.me.mantas.eternity.tests.TestHarness;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class SavedGameOpenerTest extends TestHarness {
	private static final String DESERIALIZATION_ERR = "{\"error\":\"DESERIALIZATION_ERR\"}";

	@Test
	public void mobileObjectsFileNotExists () {
		CefQueryCallback mockCallback = mock(CefQueryCallback.class);
		SavedGameOpener cls = new SavedGameOpener("404", mockCallback);
		cls.run();

		verify(mockCallback).success("{\"error\":\"NOT_EXISTS\"}");
	}

	@Test
	public void deserializationError () throws IOException {
		CefQueryCallback mockCallback = mock(CefQueryCallback.class);
		Optional<File> saveDir = EKUtils.createTempDir(PREFIX);
		assertTrue(saveDir.isPresent());

		Files.createFile(
			new File(saveDir.get(), "MobileObjects.save").toPath());

		SavedGameOpener cls =
			new SavedGameOpener(saveDir.get().getAbsolutePath(), mockCallback);

		cls.run();
		verify(mockCallback).success("{\"error\":\"DESERIALIZATION_ERR\"}");
	}

	@Test
	public void saveGameOpened () throws URISyntaxException, IOException {
		final Settings mockSettings = mockSettings();
		final JSONObject mockJSON = mock(JSONObject.class);
		final CefQueryCallback mockCallback = mock(CefQueryCallback.class);
		final File resources = new File(getClass().getResource("/").toURI());
		final SavedGameOpener cls =
			new SavedGameOpener(resources.getAbsolutePath(), mockCallback);

		final File extractedFile =
			new File(getClass().getResource("/SavedGameOpenerExtracted.json").toURI());

		final String extractedJSONString = FileUtils.readFileToString(extractedFile);
		// We stick the JSON string in a JSON object to remove all whitespace.
		final JSONObject extractedJSON = new JSONObject(extractedJSONString);

		mockSettings.json = mockJSON;
		when(mockJSON.getString("gameLocation")).thenReturn(
			new File(resources, "SavedGameOpenerTest").getAbsolutePath());

		cls.run();
		verify(mockCallback).success(extractedJSON.toString());
	}

	private PacketDeserializer mockDeserializer (final Environment mockEnvironment) {
		final PacketDeserializerFactory mockFactory = mock(PacketDeserializerFactory.class);
		final PacketDeserializer mockDeserializer = mock(PacketDeserializer.class);

		when(mockEnvironment.packetDeserializer()).thenReturn(mockFactory);
		when(mockFactory.forFile(any(File.class))).thenReturn(mockDeserializer);

		return mockDeserializer;
	}

	@Test
	public void deserializeTestDeserializationError () throws FileNotFoundException {
		final Environment mockEnvironment = mockEnvironment();
		final PacketDeserializer mockDeserializer = mockDeserializer(mockEnvironment);
		final File mockMobileObjectsFile = mock(File.class);
		final CefQueryCallback mockCallback = mock(CefQueryCallback.class);

		when(mockDeserializer.deserialize()).thenReturn(Optional.empty());
		when(mockMobileObjectsFile.getAbsolutePath()).thenReturn("404");

		final SavedGameOpener savedGameOpener = new SavedGameOpener("404", mockCallback);
		final ExposedClass exposedOpener = expose(savedGameOpener);
		exposedOpener.call("deserialize", mockMobileObjectsFile);

		verify(mockCallback).success(DESERIALIZATION_ERR);
	}

	@Test
	public void deserializeTestThrowsException () throws FileNotFoundException {
		final Environment mockEnvironment = mockEnvironment();
		final PacketDeserializer mockDeserializer = mockDeserializer(mockEnvironment);
		final File mockMobileObjectsFile = mock(File.class);
		final CefQueryCallback mockCallback = mock(CefQueryCallback.class);

		when(mockMobileObjectsFile.getAbsolutePath()).thenReturn("404");
		doThrow(new FileNotFoundException()).when(mockDeserializer).deserialize();

		final SavedGameOpener savedGameOpener = new SavedGameOpener("404", mockCallback);
		final ExposedClass exposedOpener = expose(savedGameOpener);
		exposedOpener.call("deserialize", mockMobileObjectsFile);

		verify(mockCallback).success(DESERIALIZATION_ERR);
	}

	@Test
	@SuppressWarnings("unchecked")
	public void extractCharactersTest () {
		final CefQueryCallback mockCallback = mock(CefQueryCallback.class);
		final Property noObjectID = mock(Property.class);
		final Property startsWithCompanion = mock(Property.class);
		final Property startsWithPlayer = mock(Property.class);

		final List<Property> gameObjects = new ArrayList<Property>(){{
			add(noObjectID);
			add(startsWithCompanion);
			add(startsWithPlayer);
		}};

		final ObjectPersistencePacket noObjectIDPacket = new ObjectPersistencePacket();
		noObjectIDPacket.ObjectName = "";
		noObjectIDPacket.ObjectID = null;
		noObjectID.obj = noObjectIDPacket;

		final ObjectPersistencePacket startsWithCompanionPacket = new ObjectPersistencePacket();
		startsWithCompanionPacket.ObjectName = "Companion_A";
		startsWithCompanionPacket.ObjectID = "Key_A";
		startsWithCompanion.obj = startsWithCompanionPacket;

		final ObjectPersistencePacket startsWithPlayerPacket = new ObjectPersistencePacket();
		startsWithPlayerPacket.ObjectName = "Player_B";
		startsWithPlayerPacket.ObjectID = "Key_B";
		startsWithPlayer.obj = startsWithPlayerPacket;

		final SavedGameOpener savedGameOpener = new SavedGameOpener("404", mockCallback);
		final ExposedClass exposedOpener = expose(savedGameOpener);

		final Map<Object, Class> argMap = new HashMap<Object, Class>(){{
			put(gameObjects, List.class);
		}};

		final Map<String, Property> characters =
			(Map<String, Property>) exposedOpener.call("extractCharacters", argMap);

		assertEquals(2, characters.size());
		assertSame(startsWithCompanion, characters.get("Key_A"));
		assertSame(startsWithPlayer, characters.get("Key_B"));
	}

	@Test
	public void extractPortraitTestSelectsFirst () {
		final CefQueryCallback mockCallback = mock(CefQueryCallback.class);
		final ObjectPersistencePacket packet = new ObjectPersistencePacket();
		final ComponentPersistencePacket firstComponent = new ComponentPersistencePacket();
		final ComponentPersistencePacket secondComponent = new ComponentPersistencePacket();

		firstComponent.TypeString = "Portrait";
		secondComponent.TypeString = "Portrait";
		firstComponent.Variables = new HashMap<>();
		secondComponent.Variables = new HashMap<String, Object>() {{
			put("m_textureLargePath", "png");
		}};

		packet.ComponentPackets = new ComponentPersistencePacket[]{
			firstComponent
			, secondComponent
		};

		mockSettings();

		final SavedGameOpener savedGameOpener = new SavedGameOpener("404", mockCallback);
		final ExposedClass exposedOpener = expose(savedGameOpener);
		final String result = (String) exposedOpener.call("extractPortrait", packet, false);

		assertEquals("", result);
	}

	@Test
	public void extractPortraitTestNoGameLocation () {
		final Settings mockSettings = mockSettings();
		final CefQueryCallback mockCallback = mock(CefQueryCallback.class);
		final ObjectPersistencePacket packet = new ObjectPersistencePacket();
		final ComponentPersistencePacket component = new ComponentPersistencePacket();

		component.TypeString = "Portrait";
		component.Variables = new HashMap<String, Object>(){{
			put("m_textureLargePath", "png");
		}};

		mockSettings.json = new JSONObject();
		packet.ComponentPackets = new ComponentPersistencePacket[]{ component };

		final SavedGameOpener savedGameOpener = new SavedGameOpener("404", mockCallback);
		final ExposedClass exposedOpener = expose(savedGameOpener);
		final String result = (String) exposedOpener.call("extractPortrait", packet, false);

		assertEquals("", result);
	}

	@Test
	public void extractPortraitTestPortraitNotFound () throws URISyntaxException {
		final Settings mockSettings = mockSettings();
		final Logger mockLogger = interceptLogging(SavedGameOpener.class);
		final CefQueryCallback mockCallback = mock(CefQueryCallback.class);
		final ObjectPersistencePacket packet = new ObjectPersistencePacket();
		final ComponentPersistencePacket component = new ComponentPersistencePacket();
		final File testResources = new File(getClass().getResource("/").toURI());

		component.TypeString = "Portrait";
		component.Variables = new HashMap<String, Object>(){{
			put("m_textureLargePath", "../404");
		}};

		mockSettings.json = new JSONObject().put("gameLocation", testResources.getAbsolutePath());
		packet.ComponentPackets = new ComponentPersistencePacket[]{ component };

		final SavedGameOpener savedGameOpener = new SavedGameOpener("404", mockCallback);
		final ExposedClass exposedOpener = expose(savedGameOpener);
		final String result = (String) exposedOpener.call("extractPortrait", packet, false);

		final ArgumentCaptor<String> argument = ArgumentCaptor.forClass(String.class);
		verify(mockLogger).error(anyString(), argument.capture());
		assertEquals("", result);
		assertEquals(new File(testResources, "404").getAbsolutePath(), argument.getValue());
	}

	@Test
	public void extractPortraitTestUnmappedCompanion () throws URISyntaxException {
		final Settings mockSettings = mockSettings();
		final Logger mockLogger = interceptLogging(SavedGameOpener.class);
		final CefQueryCallback mockCallback = mock(CefQueryCallback.class);
		final ObjectPersistencePacket packet = new ObjectPersistencePacket();
		final ComponentPersistencePacket component = new ComponentPersistencePacket();
		final File testResources = new File(getClass().getResource("/").toURI());
		final String companionPortraitPath = Environment.getInstance().getCompanionPortraitPath();
		final String substitutedPath = String.format(companionPortraitPath, "unmapped");
		final File portraitFile =
			Paths.get(testResources.toURI())
				.resolve(Environment.PILLARS_DATA_DIR)
				.resolve(substitutedPath)
				.toFile();

		mockSettings.json = new JSONObject().put("gameLocation", testResources.getAbsolutePath());
		packet.ObjectName = "Companion_Unmapped";
		component.TypeString = "Portrait";
		component.Variables = new HashMap<>();
		packet.ComponentPackets = new ComponentPersistencePacket[]{ component };

		final SavedGameOpener savedGameOpener = new SavedGameOpener("404", mockCallback);
		final ExposedClass exposedOpener = expose(savedGameOpener);
		final String result = (String) exposedOpener.call("extractPortrait", packet, true);

		final ArgumentCaptor<String> argument = ArgumentCaptor.forClass(String.class);
		verify(mockLogger).error(anyString(), argument.capture());
		assertEquals("", result);
		assertEquals(portraitFile.getAbsolutePath(), argument.getValue());
	}

	@Test
	public void extractPortraitTestMappedCompanion () throws URISyntaxException {
		final Settings mockSettings = mockSettings();
		final Logger mockLogger = interceptLogging(SavedGameOpener.class);
		final CefQueryCallback mockCallback = mock(CefQueryCallback.class);
		final ObjectPersistencePacket packet = new ObjectPersistencePacket();
		final ComponentPersistencePacket component = new ComponentPersistencePacket();
		final File testResources = new File(getClass().getResource("/").toURI());
		final String companionPortraitPath = Environment.getInstance().getCompanionPortraitPath();
		final String substitutedPath = String.format(companionPortraitPath, "grieving_mother");
		final File portraitFile =
			Paths.get(testResources.toURI())
				.resolve(Environment.PILLARS_DATA_DIR)
				.resolve(substitutedPath)
				.toFile();

		mockSettings.json = new JSONObject().put("gameLocation", testResources.getAbsolutePath());
		packet.ObjectName = "Companion_GM";
		component.TypeString = "Portrait";
		component.Variables = new HashMap<>();
		packet.ComponentPackets = new ComponentPersistencePacket[]{ component };

		final SavedGameOpener savedGameOpener = new SavedGameOpener("404", mockCallback);
		final ExposedClass exposedOpener = expose(savedGameOpener);
		final String result = (String) exposedOpener.call("extractPortrait", packet, true);

		final ArgumentCaptor<String> argument = ArgumentCaptor.forClass(String.class);
		verify(mockLogger).error(anyString(), argument.capture());
		assertEquals("", result);
		assertEquals(portraitFile.getAbsolutePath(), argument.getValue());
	}

	@Test
	public void detectDeadTest () {
		final CefQueryCallback mockCallback = mock(CefQueryCallback.class);
		final ObjectPersistencePacket packet = new ObjectPersistencePacket();
		final ComponentPersistencePacket component = new ComponentPersistencePacket();

		component.TypeString = "Health";
		component.Variables = new HashMap<>();
		packet.ComponentPackets = new ComponentPersistencePacket[]{ component };

		final SavedGameOpener savedGameOpener = new SavedGameOpener("404", mockCallback);
		final ExposedClass exposedOpener = expose(savedGameOpener);
		boolean result = (boolean) exposedOpener.call("detectDead", packet);
		assertFalse(result);

		component.Variables = new HashMap<String, Object>(){{
			put("CurrentHealth", 1f);
		}};

		result = (boolean) exposedOpener.call("detectDead", packet);
		assertFalse(result);

		component.Variables = new HashMap<String, Object>(){{
			put("CurrentHealth", 0f);
		}};

		result = (boolean) exposedOpener.call("detectDead", packet);
		assertTrue(result);
	}

	@Test
	public void extractNameTest () {
		final CefQueryCallback mockCallback = mock(CefQueryCallback.class);
		final ObjectPersistencePacket packet = new ObjectPersistencePacket();
		final SavedGameOpener savedGameOpener = new SavedGameOpener("404", mockCallback);
		final ExposedClass exposedOpener = expose(savedGameOpener);

		packet.ObjectName = "NoUnderscore";
		String result = (String) exposedOpener.call("extractName", packet);
		assertEquals("", result);

		packet.ObjectName = "Player_HasUnderscore";
		result = (String) exposedOpener.call("extractName", packet);
		assertEquals("HasUnderscore", result);

		packet.ObjectName = "Player_HasBracket(Clone)_1";
		result = (String) exposedOpener.call("extractName", packet);
		assertEquals("HasBracket", result);
	}

	@Test
	@SuppressWarnings("unchecked")
	public void extractCharacterStatsTest () {
		final CefQueryCallback mockCallback = mock(CefQueryCallback.class);
		final ObjectPersistencePacket packet = new ObjectPersistencePacket();
		final ComponentPersistencePacket notStatsComponent = new ComponentPersistencePacket();
		final ComponentPersistencePacket statsComponent = new ComponentPersistencePacket();
		final SavedGameOpener savedGameOpener = new SavedGameOpener("404", mockCallback);
		final ExposedClass exposedOpener = expose(savedGameOpener);

		notStatsComponent.TypeString = "NotCharacterStats";
		statsComponent.TypeString = "CharacterStats";
		statsComponent.Variables = new HashMap<String, Object>(){{
			put("Integer", 1);
			put("Float", 1f);
			put("String", "1");
			put("UnsignedInteger", UnsignedInteger.valueOf(1L));
		}};

		packet.ComponentPackets = new ComponentPersistencePacket[]{ notStatsComponent };

		Optional<Map<String, Object>> result =
			(Optional<Map<String, Object>>) exposedOpener.call("extractCharacterStats", packet);

		assertFalse(result.isPresent());

		packet.ComponentPackets = new ComponentPersistencePacket[]{
			notStatsComponent
			, null
			, statsComponent
		};

		result =
			(Optional<Map<String, Object>>) exposedOpener.call("extractCharacterStats", packet);

		assertTrue(result.isPresent());
		assertEquals(1, result.get().get("Integer"));
		assertEquals(1f, (float) result.get().get("Float"), 1e-6);
		assertEquals("1", result.get().get("String"));
		assertNull(result.get().get("UnsignedInteger"));
	}

	@Test
	public void detectCompanionTest () {
		final CefQueryCallback mockCallback = mock(CefQueryCallback.class);
		final ObjectPersistencePacket packet = new ObjectPersistencePacket();
		final SavedGameOpener savedGameOpener = new SavedGameOpener("404", mockCallback);
		final ExposedClass exposedOpener = expose(savedGameOpener);

		packet.ObjectName = "Companion_Calisca";
		boolean result = (boolean) exposedOpener.call("detectCompanion", packet);
		assertTrue(result);

		packet.ObjectName = "Companion_Generic_(Clone)_1";
		result = (boolean) exposedOpener.call("detectCompanion", packet);
		assertFalse(result);

		packet.ObjectName = "Player_Fyorl";
		result = (boolean) exposedOpener.call("detectCompanion", packet);
		assertFalse(result);
	}

	private Tuple2<Logger, ExposedClass> setupExtractCurrency () {
		final CefQueryCallback mockCallback = mock(CefQueryCallback.class);
		final Logger mockLogger = interceptLogging(SavedGameOpener.class);
		final SavedGameOpener opener = new SavedGameOpener("", mockCallback);
		final ExposedClass exposedOpener = expose(opener);
		return new Tuple2<>(mockLogger, exposedOpener);
	}

	@Test
	public void extractCurrencyTestNoPlayerPacket () {
		final Tuple2<Logger, ExposedClass> setup = setupExtractCurrency();
		final Map<Object, Class> argMap = new HashMap<Object, Class>() {{
			put(new ArrayList<Property>(), List.class);
		}};

		assertEquals(0f, (float) setup.v2().call("extractCurrency", argMap), 0f);
		verify(setup.v1()).error("Unable to find player mobile object.%n");
	}

	@Test
	public void extractCurrencyTestNoPlayerInventoryComponent () {
		final Tuple2<Logger, ExposedClass> setup = setupExtractCurrency();
		final Property property = mock(Property.class);
		final ObjectPersistencePacket playerPacket = mock(ObjectPersistencePacket.class);
		final List<Property> gameObjects = new ArrayList<Property>() {{add(property);}};
		final Map<Object, Class> argMap = new HashMap<Object, Class>() {{
			put(gameObjects, List.class);
		}};

		property.obj = playerPacket;
		playerPacket.ObjectName = "Player_Elenor";
		playerPacket.ComponentPackets = new ComponentPersistencePacket[0];

		assertEquals(0f, (float) setup.v2().call("extractCurrency", argMap), 0f);
		verify(setup.v1()).error("Unable to find PlayerInventory component.");
	}

	@Test
	public void extractCurrencyTestNoCurrencyTotalValue () {
		final Tuple2<Logger, ExposedClass> setup = setupExtractCurrency();
		final Property property = mock(Property.class);
		final ObjectPersistencePacket playerPacket = mock(ObjectPersistencePacket.class);
		final ComponentPersistencePacket inventoryComponent =
			mock(ComponentPersistencePacket.class);

		final List<Property> gameObjects = new ArrayList<Property>() {{add(property);}};
		final Map<Object, Class> argMap = new HashMap<Object, Class>() {{
			put(gameObjects, List.class);
		}};

		property.obj = playerPacket;
		playerPacket.ObjectName = "Player_Elenor";
		playerPacket.ComponentPackets = new ComponentPersistencePacket[] {inventoryComponent};
		inventoryComponent.TypeString = "PlayerInventory";
		inventoryComponent.Variables = new HashMap<>();

		assertEquals(0f, (float) setup.v2().call("extractCurrency", argMap), 0f);
		verify(setup.v1()).error("Unable to find currencyTotalValue in PlayerInventory component.");
	}

	@Test
	public void extractCurrencyTest () {
		final Tuple2<Logger, ExposedClass> setup = setupExtractCurrency();
		final Property property = mock(Property.class);
		final ObjectPersistencePacket playerPacket = mock(ObjectPersistencePacket.class);
		final CurrencyValue currencyValue = new CurrencyValue();
		final ComponentPersistencePacket inventoryComponent =
			mock(ComponentPersistencePacket.class);

		final List<Property> gameObjects = new ArrayList<Property>() {{add(property);}};
		final Map<Object, Class> argMap = new HashMap<Object, Class>() {{
			put(gameObjects, List.class);
		}};

		currencyValue.v = 3.14159f;
		property.obj = playerPacket;
		playerPacket.ObjectName = "Player_Elenor";
		playerPacket.ComponentPackets = new ComponentPersistencePacket[] {inventoryComponent};
		inventoryComponent.TypeString = "PlayerInventory";
		inventoryComponent.Variables = new HashMap<String, Object>() {{
			put("currencyTotalValue", currencyValue);
		}};

		assertEquals(3.14159f, (float) setup.v2().call("extractCurrency", argMap), 1e-6);
	}
}
